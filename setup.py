#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "django-simple-ads",
    version = "1.0.0",
    url = 'https://github.com/ondrejsika/django-simple-ads',
    license = 'MIT',
    description = 'Simple ads manager.',
    author = 'Ondrej Sika',
    author_email = 'ondrej@ondrejsika.com',
    packages = find_packages(),
)
