from django.contrib import admin

from .models import Ad


class AdAdmin(admin.ModelAdmin):
    list_display = (
        'pk',
        'size',
        'name',
    )
    list_filter = (
        'size',
    )


admin.site.register(Ad, AdAdmin)
