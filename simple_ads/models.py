from django.db import models


class Ad(models.Model):
    size = models.CharField(max_length=16)
    name = models.CharField(max_length=64, blank=True, null=True)
    img = models.ImageField(upload_to='simpleads/ad/img', blank=True, null=True)
    link = models.URLField(blank=True, null=True)
    html = models.TextField(blank=True, null=True)

    def get_ad_code(self):
        if self.html:
            return u'%s' % self.html
        if self.img and self.link:
            return u'<a href="%s" target="_blank"><img src="%s"></a>' % (self.link, self.img.url)
        if self.img:
            return u'<img src="%s">' % self.img.url
        return u''

    def __unicode__(self):
        return u'%s %s' % (self.size, self.name or self.pk)
