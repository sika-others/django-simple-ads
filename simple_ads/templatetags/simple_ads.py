import random

from django import template

from ..models import Ad


register = template.Library()


@register.simple_tag
def random_ad_by_size(size):
    w, h = size.split('x')
    try:
        html = random.choice(list(Ad.objects.filter(size=size))).get_ad_code()
    except IndexError:
        html = u'<div style="background-color: #AAA; width: %spx; height: %spx; margin: 0;">%s</div>' % (w, h, size)
    return u'<div style="width: %spx; height: %spx; margin: 0; padding: 0;">%s</div>' % (w, h, html)
